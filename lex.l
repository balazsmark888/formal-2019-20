%{
#include<iostream>
using namespace std; 

int oszlop = 0;


%}

%option noyywrap 
%option yylineno


%%

N   {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (int)"<< endl;
    oszlop += yyleng;
}

R { 
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< " (float)"<< endl;
    oszlop += yyleng;
}

plus {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (+)"<< endl;
    oszlop += yyleng; 
}

minus {
    
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (-)"<< endl;
    oszlop += yyleng; 
}

multipl {
    
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (*)"<< endl;
    oszlop += yyleng; 
}

"->" {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (to)"<< endl;
    oszlop += yyleng;
}


\>= {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl;
    oszlop += yyleng;
}

\<= {
    
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl;
    oszlop += yyleng;
}

==  {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl;
    oszlop += yyleng;
}
\< {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl;
    oszlop += yyleng;
}

\> {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl;
    oszlop += yyleng;
}

= {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< endl; 
    oszlop += yyleng;
}
\?  {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (if begin)" <<endl;
    oszlop += yyleng;
}
"yes" {
    
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (then in if)" << endl;
    oszlop += yyleng;
}
"no" {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (else)" <<endl;
    oszlop += yyleng;
}
"end" {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (end if/while/for)" <<endl;
    oszlop += yyleng;
}
\(  {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (while/for exp begin)" <<endl;
    oszlop += yyleng;
}

\)  {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<< " (while/for exp end)"<< endl;
    oszlop += yyleng;
}

\~ {

    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (not)"<< endl;
    oszlop += yyleng;
}

\* {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (or)" <<endl;
    oszlop += yyleng;
}


" "|\t {
    oszlop++; 
}

\n {
    oszlop =1;

}
[0-9]+(\.[0-9]+)?   {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<"(szam)"<< endl;
    oszlop += yyleng;
}
[A-Za-z_.]+  {
    
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (valtozonev)"<< endl;
    oszlop += yyleng;       
}

. {
    cout << "[sor: "<< yylineno << ", oszlop: "<< oszlop<< ", hossz: "<<yyleng<< "] "<<yytext<<" (lexikalis hiba)"<< endl;
    oszlop += yyleng;
}

%%

int main()
{
    yylex();
}